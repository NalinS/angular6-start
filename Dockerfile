FROM node:alpine as builder

RUN apk update && apk add --no-cache make git

RUN mkdir -p /usr/share/app

WORKDIR /usr/share/app

COPY package*.json /usr/share/app

RUN cd /usr/share/app && npm set progress=false && npm install && npm install -g @angular/cli@6.0.3

COPY . /usr/share/app

RUN cd /usr/share/app && npm run build

FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf

COPY --from=builder usr/share/app/dist/angular6-start/* /usr/share/nginx/html/

EXPOSE 3000

CMD ["nginx", "-g", "daemon off;"]

